package com.digitallschool.training.spiders.candidatemodel;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



@Service
public class CandidateService {
	@Autowired
	CandidateRepository candidaterepository;

	public List<Candidate> allCandidates() throws SQLException
	{
		return candidaterepository.allCandidates();
	}
	public boolean addCandidate(Candidate candidate) {
		return candidaterepository.addCandidate(candidate);
	}

	public boolean deleteCandidate(int candidateId) {
		return candidaterepository.deleteCandidate(candidateId);
	}

	public boolean updateCandidate(int candidateId, Candidate candidate) {
		return candidaterepository.updateCandidate(candidateId, candidate);
	}
	public Candidate findCandidate(int candidateId)
	{
		return candidaterepository.findCandidate(candidateId);
	}
}
