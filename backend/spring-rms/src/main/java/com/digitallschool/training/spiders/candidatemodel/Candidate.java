package com.digitallschool.training.spiders.candidatemodel;

import java.util.List;

public class Candidate {
	private int candidateId;
	private String firstName;
	private String lastName;
	private long mobile;
	private long alternativeMobile;
	private String email;
	private int totalExperience;
	private int relavantExperience;
	private List<String> primarySkills;
	private List<String> secondarySkills;
	private String qualification;
	private String additionalQualification;
	private double expectedCTC;
	private double currentCTC;
	private String positionApplied;
	private List<String> address;
	private String gender;
	private String dateOfBirth;

	public Candidate() {
		super();
	}

	public int getCandidateId() {
		return candidateId;
	}

	public void setCandidateId(int candidateId) {
		this.candidateId = candidateId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public long getMobile() {
		return mobile;
	}

	public void setMobile(long mobile) {
		this.mobile = mobile;
	}

	public long getAlternativeMobile() {
		return alternativeMobile;
	}

	public void setAlternativeMobile(long alternativeMobile) {
		this.alternativeMobile = alternativeMobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTotalExperience() {
		return totalExperience;
	}

	public void setTotalExperience(int totalExperience) {
		this.totalExperience = totalExperience;
	}

	public int getRelavantExperience() {
		return relavantExperience;
	}

	public void setRelavantExperience(int relavantExperience) {
		this.relavantExperience = relavantExperience;
	}

	public List<String> getPrimarySkills() {
		return primarySkills;
	}

	public void setPrimarySkills(List<String> primarySkills) {
		this.primarySkills = primarySkills;
	}

	public List<String> getSecondarySkills() {
		return secondarySkills;
	}

	public void setSecondarySkills(List<String> secondarySkills) {
		this.secondarySkills = secondarySkills;
	}

	public String getQualification() {
		return qualification;
	}

	public void setQualification(String qualification) {
		this.qualification = qualification;
	}

	public String getAdditionalQualification() {
		return additionalQualification;
	}

	public void setAdditionalQualification(String additionalQualification) {
		this.additionalQualification = additionalQualification;
	}

	public double getExpectedCTC() {
		return expectedCTC;
	}

	public void setExpectedCTC(double expectedCTC) {
		this.expectedCTC = expectedCTC;
	}

	public double getCurrentCTC() {
		return currentCTC;
	}

	public void setCurrentCTC(double currentCTC) {
		this.currentCTC = currentCTC;
	}

	public String getPositionApplied() {
		return positionApplied;
	}

	public void setPositionApplied(String positionApplied) {
		this.positionApplied = positionApplied;
	}

	public List<String> getAddress() {
		return address;
	}

	public void setAddress(List<String> address) {
		this.address = address;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

}
