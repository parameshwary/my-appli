package com.digitallschool.training.spiders.candidatemodel;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.digitallschool.training.spiders.model.Interview;

@RestController
@CrossOrigin("*")
@RequestMapping("/candidate")
public class CandidateController {

	@Autowired
	CandidateService candidateservice;

	@GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Candidate> allCandidates() throws SQLException {
//		List<Candidate> lt = candidateservice.allCandidates();
//		lt.forEach(e -> System.out.println(e.getFirstName()));
		return candidateservice.allCandidates();
	}
	@GetMapping("{cid}")
	public Candidate findCandidate(@PathVariable int cid) {
		Candidate candidate=null;
		candidate=candidateservice.findCandidate(cid);
		return candidate;
	}

	@PostMapping
	public ResponseEntity<?> addCandidate(@RequestBody Candidate candidate, BindingResult result) {

		candidate.getPrimarySkills().forEach(System.out::println);
		if (!result.hasErrors() && candidateservice.addCandidate(candidate)) {
			return ResponseEntity.ok("Successfully Added");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body("Incomplete or Invalid Data: " + result.toString());
		}
	}

	@DeleteMapping({ "/{candidateId}" })
	public ResponseEntity<String> deleteCandidate(@PathVariable int candidateId) {
		candidateservice.deleteCandidate(candidateId);
		return ResponseEntity.ok("Successfully Deleted");
	}

	@PutMapping
	public ResponseEntity updateCandidate(@RequestBody Candidate candidate, BindingResult result) {
		int id=candidate.getCandidateId();
		if (!result.hasErrors() && candidateservice.updateCandidate(id, candidate)) {
			return ResponseEntity.ok("Successfully Updated");
		} else {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED)
					.body("Incomplete or Invalid Data: " + result.toString());
		}
	}

	@ModelAttribute("candidate")
	public Interview setInterview() {
		return new Interview();
	}

}
